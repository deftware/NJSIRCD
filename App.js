/**
 * Copyright (C) 2017 Deftware All Rights Reserved
 */

const fs = require('fs');
const logserver = require("./logs.js");
const ircserver = require("./server.js");
const logger = new logserver.LogServer(ircserver);

if (fs.existsSync('./data/ssl/cert.pem')) {
    logger.start.call(logger);
}

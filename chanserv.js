class ChanServ {

    constructor() {
        this.nick = "ChanServ";
        this.host = "chanserv";
        this.realname = "ChanServ Bot";
    }

    shouldShadowBan(nick, realname, whitelisted_nicks, isInMap, log) {
        if (!isInMap(nick, whitelisted_nicks)) {
            // Your logic here
        }
        return false;
    }

}

module.exports = { ChanServ };

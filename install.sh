#!/bin/bash

echo "Installing NJSIRCD..."

# Clone git
git clone https://gitlab.com/deftware/NJSIRCD.git
cd NJSIRCD

# Install Node modules
echo "Installing dependencies..."
npm install

# Done
echo "Done, if you want to use SSL drop your cert and key in the SSL folder."
echo "To start the server run \"node App\" or \"npm start\""

/**
 * Copyright (C) 2017 Deftware All Rights Reserved
 */

const express = require('express');
const session = require('express-session');
const fs = require('fs');
const http = require('http');
const https = require('https');
const logs = fs.readFileSync("./logs.html").toString();
const login = fs.readFileSync("./login.html").toString();
const bodyParser = require('body-parser');

let srv;

class LogServer {

    constructor(server) {
        this.server = server;
        this.app = express();
        srv = server;
        parseLog();
    }

    start() {
        this.express = https.createServer(this.server.options, this.app).listen(8594, function(){
            getServer().log("Log server started on port 8594");
        });
        this.app.use(bodyParser.json());       // to support JSON-encoded bodies
        this.app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
            extended: true
        })); 
        this.app.use(session({
            resave: false, // don't save session if unmodified
            saveUninitialized: false, // don't create session until something stored
            secret: '05797109268550662546-73044937283019968875'
        }));
        this.app.get('/login', function (req, res) {
            res.writeHead(200);
            res.end(login + "\n");
        });
        this.app.post('/login', function(req, res) {
            if (authenticate(req.body.username, req.body.password)) {
                req.session.regenerate(function(){
                    req.session.user = req.body.username;
                    res.redirect('/');
                });
            } else {
                res.writeHead(403);
                res.end(login + "\n");
            }
        });
        this.app.get('/logout', function(req, res) {
            req.session.destroy(function(){
                res.redirect('/');
            });
        });
        this.app.get('/get', restrict, function (req, res) {
            res.writeHead(200);
            res.end(parseLog(req.session.user) + "\n");
        });
        this.app.get('/ban', restrict, function (req, res) {
            res.writeHead(200);
            let ip = req.url.split("?")[1].split("&")[0];
            let channel = req.url.split("?")[1].split("&")[1];
			let nick = req.url.split("?")[1].split("&")[2];
            srv.banUser("#" + channel, ip, nick);
            res.end("success" + "\n");
        });
        this.app.get('/', restrict, function (req, res) {
            res.writeHead(200);
            res.end(logs.replace("%user%", req.session.user) + "\n");
        });
    }

    stop() {

    }

}

function authenticate(username, password) {
    if (srv.getVar(username + "_hash") === srv.hashString(password)) {
        return true;
    }
    return false;
}

function restrict(req, res, next) {
    if (req.session.user) {
        next();
    } else {
        req.session.error = 'Access denied!';
        res.redirect('/login');
    }
}

function getCurrentDate() {
    const dateobj= new Date() ;
    const month = dateobj.getMonth() + 1;
    const day = dateobj.getDate();
    const year = dateobj.getFullYear();
    return year + "-" + month + "-" + day;
}

function parseLog(req_username) {
    if (!fs.existsSync("./logs/" + getCurrentDate() + ".log")) {
        return "Empty log";
    }
    let messages = "";
    let log = fs.readFileSync("./logs/" + getCurrentDate() + ".log").toString();
    log = log.split(/\[/); //log.split(/(\[[0-9]?[0-9]:[0-9]?[0-9]:[0-9]?[0-9]\]\s)/);
    log.reverse();
    for (let line in log) {
        line = log[line].toString();
        if (!(line.includes("{ nick: '") && line.includes("message: '") && line.includes("host: '"))) {
            continue;
        } 
        let message = line.match(new RegExp("message: '(.*?)'"))[0];
        message = message.substring("message: '".length, message.length - 1);
        let sender = line.match(new RegExp("nick: '(.*?)'"))[0];
        sender = sender.substring("nick: '".length, sender.length - 1);
        let ip = line.match(new RegExp("host: '(.*?)'"))[0];
        ip = ip.substring("host: '".length, ip.length - 1);
        let channel = line.match(new RegExp("to: '(.*?)'"))[0];
        channel = channel.substring("to: '".length, channel.length - 1);
        let date = line.split("]")[0];
        sender = "<a href=\"#\" onclick=\"banUser('/ban?" + ip + "&" + channel.substring(1) + "&" + sender + "')\">" + sender + "</a>";
        // Make sure we're op on this channel
        if (srv.isUserOpOnChannel(req_username, channel)) {
            messages += getHtml(sender + " [" + date + "] => " + channel, escapeHtml(message));
        }    
    }
    if (messages === "") {
        messages = "Empty log (Or you are not permitted to view logs)";
    }
    return messages;
}

function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

function getHtml(sender, message) {
    let string = "<div class=\"panel panel-default\">";
    string += "\n<div class=\"panel-heading\">" + sender + "</div>";
    string += "\n<div class=\"panel-body\">" + message + "</div>";
    string += "\n</div>";
    return string;
}

function getServer() {
    return srv;
}

module.exports = {LogServer};